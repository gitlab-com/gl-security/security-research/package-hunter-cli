**This project has been archived. Future development of this project will happen at https://gitlab.com/gitlab-org/security-products/package-hunter-cli/.**

# Package Hunter CLI

Behavorial monitoring for identifying malicious dependencies.

This is the Package Hunter CLI client. It is useful for running Package Hunter jobs in CI pipelines and from your local computer. Requires a running [Package Hunter server](https://gitlab.com/gitlab-com/gl-security/security-research/package-hunter).

# Getting Started

## GitLab CI

```
include: "https://gitlab.com/gitlab-org/security-products/package-hunter-cli/ci/template/Package-Hunter.gitlab-ci.yml"

.package_hunter-base:
  variables:
    HTR_user: "$PACKAGE_HUNTER_USER"
    HTR_pass: "$PACKAGE_HUNTER_PASS"
    HTR_host: "https://package-hunter-server.example"
```

Set `PACKAGE_HUNTER_USER` and `PACKAGE_HUNTER_PASS` in your project settings according to your Package Hunter server configuration. For more information on configuring the Package Hunter server, refer to the [docs](https://gitlab.com/gitlab-org/security-products/package-hunter/-/blob/master/README.md#installation). And for more information on setting CI variables, see the [CI variable docs](https://docs.gitlab.com/ee/ci/variables/). 

## Locally

### Docker (recommended)

Requirements:
- Docker

```sh
docker run --rm registry.gitlab.com/gitlab-com/gl-security/security-research/package-hunter-cli analyze --help
```

### From Source

Requirements:
- Node.js v12.16 or newer

```sh
git clone https://gitlab.com/gitlab-com/gl-security/security-research/package-hunter-cli.git
cd package-hunter-cli
npm install
DEBUG=pkgs* node cli.js analyze --help
```

## Usage

To analyze a project, create an archive with the project sources and pass it to the CLI client:

```sh
tar czvf gitlab.tgz ~/git/gitlab

# using Docker
docker run --rm \
  -v "${PWD}:/usr/src/app" \
  --env "DEBUG=pkgs*" \
  --env "HTR_user=someuser" --env "HTR_pass=somepass" \
  registry.gitlab.com/gitlab-com/gl-security/security-research/package-hunter-cli analyze gitlab.tgz

# using Source
DEBUG=pkgs* "HTR_user=someuser" "HTR_pass=somepass" node cli.js analyze gitlab.tgz
```

The archive will be send to the Package Hunter server and any suspicious behavior will be reported back. To get an overview of the rules that were violated, you can use jq like so `... analyze gitlab.tgz | jq .result[].rule`.

The Package Hunter server requires authentication. User and password have to be provided to the client via the env var `HTR_user` and `HTR_pass` ([credentials](https://start.1password.com/open/i?a=LKATQYUATRBRDHRRABEBH4RJ5Y&v=6gq44ckmq23vqk5poqunurdgay&i=rvy4v2kvdjcpnoiihlm3vlda34&h=gitlab.1password.com) are in 1Password)

## Publishing

This project uses semantic versioning. To publish a new release, add a git tag and push it. For example, to create release `1.2.3` run:

```sh
# make sure you have the latest commits from the main branch
git pull

# use npm to increment the version. Replace "patch" with "minor" or "major" in accordance to semantic version
npm version patch

# push the changes to the remote
git push origin main --follow-tags
```

A CI job will build the docker image and publish it to the container registry. Execute the release with:

```sh
docker run registry.gitlab.com/gitlab-com/gl-security/security-research/package-hunter-cli:1.2.3

# or to run the latest release

docker run registry.gitlab.com/gitlab-com/gl-security/security-research/package-hunter-cli:latest
```

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).
