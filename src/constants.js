module.exports = {
  PACKAGE_MANAGERS: {
    YARN: 'yarn',
    BUNDLER: 'bundler'
  }
}
