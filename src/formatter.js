'use strict'

const debug = require('debug')('pkgs.client.formatter')
// const { v4: uuidv4 } = require('uuid')
const dontuseuuid = require('uuid')
JSON.canonicalize = require('canonicalize')

class Formatter {
  constructor (out, format = Formatter.HUNTER, uuidv4 = dontuseuuid.v4) {
    if (!out.write) throw new TypeError('first parameter expected to be a writeable stream')
    this.out = out
    this.uuidv4 = uuidv4

    if (format === Formatter.HUNTER) {
      this.format = Formatter.HUNTER
    } else if (format === Formatter.GITLAB) {
      this.format = Formatter.GITLAB
    } else {
      throw new Error('unknown format ' + format) // change to paramter exception
    }
  }

  print (response) {
    if (this.format === Formatter.HUNTER) {
      this._print(response) // print the raw response
      return
    }

    const alerts = []
    for (const alert of response.result) {
      alerts.push({
        id: this.uuidv4(),
        category: 'dependency_scanning',
        scanner: {
          id: 'packagehunter',
          name: 'Package Hunter'
        },
        name: alert.rule,
        message: alert.rule,
        description: alert.output,
        cve: '',
        severity: 'Unknown',
        confidence: 'Unknown',
        solution: 'The installation routine of this project exhibited suspicious behavior. ' +
          'Review if the reported behavior is expected or is the result of a malicious dependency.',
        location: {
          file: 'package.json',
          dependency: {
            package: {
              // we dont know the package name. Instead, we compute a fingerprint of the observed behavior.
              name: this._computeBehaviorIdentifier(alert)
            },
            version: ''
          }
        },
        identifiers: [
          {
            type: 'suspicious_behavior',
            name: alert.rule,
            value: alert.rule,
            url: 'https://gitlab.com/gitlab-com/gl-security/security-research/package-hunter/-/blob/65a4b5ae82b560b7bdef82d3fb358da31024a563/falco/falco_rules.local.yaml'
          }
        ],
        links: []
      })
    }

    this._print({ ...this._metadata(), vulnerabilities: alerts })
  }

  static get HUNTER () {
    return 'raw'
  }

  static get GITLAB () {
    return 'gitlab'
  }

  _print (json) {
    this.out.write(this._jsonToString(json))
  }

  _jsonToString (json) {
    return JSON.stringify(json, null, 4)
  }

  _metadata () {
    return {
      dependency_files: [],
      version: '1.0'
    }
  }

  _computeBehaviorIdentifier (alert) {
    for (const name in alert.output_fields) {
      if (name.startsWith('container.') ||
          name === 'evt.time') {
        delete alert.output_fields[name]
        continue
      }

      // source port of outgoing network connection is allocated randomly
      // remove the source port from the alert to make the location attribute stable
      // see: https://gitlab.com/gitlab-com/gl-security/security-research/package-hunter/-/issues/5
      if (alert.rule === 'Unexpected outbound connection destination' &&
        name === 'fd.name') {
        alert.output_fields[name] = this._stripSourceIpAndPort(alert.output_fields[name])
      }
    }
    return JSON.canonicalize(alert.output_fields)
  }

  _stripSourceIpAndPort (str) {
    // Falco reports the IPs in network alerts in the format src_ip:src_port->dst_ip:dst_port
    //   Example: 172.17.0.2:58346->23.94.46.191:80
    //
    // src_ip and src_port are from the server on which Package Hunter runs.
    // Strip these values and only report dst_ip and dst_port.
    try {
      return str.split('->')[1]
    } catch (err) {
      debug(`unexpected error striping src_ip and src_port from ${str}`)
      return str
    }
  }
}

module.exports = Formatter
