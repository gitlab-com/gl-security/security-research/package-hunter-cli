#!/usr/bin/env node

const { program } = require('commander')
const conf = require('rc')('package_hunter', {
  // default values
  HOST: 'https://api.package-hunter.xyz'
})

const packageMetadata = require('./package.json')
const PackageHunterClient = require('./src/client')

program
  .version(packageMetadata.version)
  .command('analyze <project>')
  .option('-f, --format <raw|gitlab>', 'format the result as "raw", which is the format returned from Package Hunter, or "gitlab", which is expected by GitLab.', 'raw')
  .option('-p, --package', '<project> is a dependency', false)
  .option('-m, --manager <yarn|bundler>', 'the package manager used in <project>', 'yarn')
  .action(async (project, cmdObj) => {
    const client = new PackageHunterClient(conf.HOST, {
      format: cmdObj.format,
      user: conf.USER,
      pass: conf.PASS
    })
    try {
      await client.analyze(project, { packageManager: cmdObj.manager, isProject: !cmdObj.package })
    } catch (err) {
      console.log(err.message)
      process.exit(1)
    }
  })

if (require.main === module) {
  program.parse(process.argv)
}

module.exports = PackageHunterClient
